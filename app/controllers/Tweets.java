package controllers;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.MutablePeriod;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import models.*;
import play.Logger;
import play.db.jpa.Blob;
import play.mvc.Controller;
import utils.TweetDateComparator;

public class Tweets extends Controller
{
	public static void index()
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		List<Tweeter> followed = getFollowings(tweeter);
		List<Tweet> tweets = getTweets(tweeter);
		
		for (Tweeter tw : followed) 
		{
			tweets.addAll(getTweets(tw));
		}	
		
		sortTweets(tweets);
		render(tweeter, tweets);
	}
	
	static List<Tweeter> getFollowings(Tweeter tweeter)
	{	    		
		List<Following> allFollowings = Following.findAll();
		ArrayList<Tweeter> followed = new ArrayList<>();

		for (Following following : allFollowings)
			if (following.follower == tweeter)
			{
				followed.add(following.followee);
			}
		return followed;
	}

	static List<Tweet> sortTweets(List<Tweet> tweets)
	{
		for (Tweet tweet : tweets)
		{
			tweet.strDate = timeSince(tweet);
		}	
		Collections.sort(tweets, new TweetDateComparator());
		return tweets;		
	}

	static List<Tweet> getTweets(Tweeter tweeter)
	{
		List<Tweet> allTweets = Tweet.findAll();
		List<Tweet> tweets = new ArrayList<Tweet>();
		for (Tweet tweet : allTweets)
		{
			if (tweet.tweeter == tweeter) 
			{
				Logger.info("Tweet" + tweet);
				tweets.add(tweet); 
			}
		}
		return tweets;
	}

	public static String timeSince(Tweet tweet)
	{		
		String min = "less than one minute ago";
		
		PeriodFormatter formatter = new PeriodFormatterBuilder()
		.printZeroNever()
		.appendWeeks()
		.appendSuffix(" week", " weeks")
		.appendSeparator(", ")
		.appendDays()
		.appendSuffix(" day", " days")
		.appendSeparator(", ")
		.appendHours()
		.appendSuffix(" hour", " hours")
		.appendSeparator(", ")
		.appendMinutes()
		.appendSuffix(" minute", " minutes")
		.toFormatter();
		
		 try {
	            DateTime now = new DateTime();
	            DateTime tweetTime = new DateTime(tweet.datestamp);
	            Interval interval = new Interval(tweetTime, now);
	            Period period = interval.toPeriod();
	            if (period.getMinutes() <= 1)
	            {
	                return min;
	            }
	            else
	            {
	                String timer = formatter.print(period) + " ago";
	                return timer;
	            }
	        } catch (Exception e) {
	            Logger.info("Error getting time");
	            return min;
	        }
	}

	public static void myTweets()
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		List<Tweet> tweets = getTweets(tweeter);
		sortTweets(tweets);
		renderTemplate("Tweets/index.html", tweeter, tweets);
	}

	public static void tweetersTweets(String id)
	{
		Tweeter tweeter = Tweeter.findById(id);
		List<Tweet> tweets = getTweets(tweeter);
		sortTweets(tweets);
		renderTemplate("Tweets/index.html", tweeter, tweets);		
	}
	
	public static void tweet()
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		Logger.info("Current tweeter " + tweeter.firstName);
		render(tweeter);
	}

	public static void newTweet(String tweetText, String charCount, Long datestamp)
	{
		Tweeter tweeter = Accounts.getCurrentTweeter();
		Tweet tweet = new Tweet(tweeter, tweetText, datestamp, charCount);
		tweet.tweeterTweetText = tweet.tweeter.firstName + " says " + tweet.tweetText;		
		Logger.info("Current tweeter " + tweeter.firstName);
		tweeter.tweets.add(tweet);
		tweet.tweeter = tweeter;
		Logger.info("tweet " + tweet);
		tweeter.save();
		tweet.save();
		index();
	}
}

