package controllers;

import java.util.List;

import models.Tweet;
import models.Tweeter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import play.mvc.Controller;
import utils.GsonStrategy;

public class TweetsAPI extends Controller 
{
	static Gson gson = new GsonBuilder()
	.setExclusionStrategies(new GsonStrategy())
	.create();

	public static void createTweet(String id, JsonElement body)
	{
	    Tweet tweet = gson.fromJson(body.toString(), Tweet.class);
	    Tweeter tweeter = Tweeter.findById(id);
	    tweeter.tweets.add(tweet);
	    tweet.tweeter = tweeter;
	    tweet.save();
	    renderJSON(gson.toJson(tweet));
	}

	public static void getTweet (String id, String tweetId)
	{
		Tweet tweet = Tweet.findById(tweetId);
		if (tweet == null)
		{
			notFound();
		}
		else
		{
			renderJSON(gson.toJson(tweet));
		}
	}

	public static void getTweets(String id)
	{
		Tweeter tweeter = Tweeter.findById(id);
		if (tweeter == null)
		{
			notFound();
		}
		renderJSON(gson.toJson(tweeter.tweets));
	}

	public static void getAllTweets()
	{
		List<Tweet> tweets = Tweet.findAll();
		renderJSON(gson.toJson(tweets));
	}


	public static void deleteTweet(String id, String tweetId)
	{
		Tweet tweet = Tweet.findById(tweetId);
		if (tweet == null)
		{
			notFound();
		}
		else
		{
			tweet.delete();
			renderJSON (gson.toJson(tweet));
		}
	}  
	
	public static void deleteAllTweets()
	{
		Tweet.deleteAll();
		renderText("success");
	}
}