package controllers;

import java.util.List;

import models.Tweeter;
import play.mvc.Controller;
import utils.GsonStrategy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

public class TweetersAPI extends Controller
{
	static Gson gson = new GsonBuilder()
	.setExclusionStrategies(new GsonStrategy())
	.create();

	public static void createTweeter(JsonElement body)
	{
		Tweeter tweeter = gson.fromJson(body.toString(), Tweeter.class);
		tweeter.save();
		renderJSON(gson.toJson(tweeter));
	}

	public static void getAllTweeters()
	{
		List<Tweeter> Tweeters = Tweeter.findAll();
		renderJSON(gson.toJson(Tweeters));	
	}

	public static void getTweeter(String id)
	{
		Tweeter tweeter = Tweeter.findById(id);
		if (tweeter == null)
		{
			notFound();
		}
		else
		{
			renderJSON(gson.toJson(tweeter));
		}
	}

	public static void deleteTweeter(String id)
	{
		Tweeter tweeter = Tweeter.findById(id);
		if (tweeter == null)
		{
			notFound("No Tweeter with ID" + id);
		}
		else
		{
			tweeter.delete();
		}
	}

	public static void deleteAllTweeters()
	{
		Tweeter.deleteAll();
		renderText("success");
	}
}
