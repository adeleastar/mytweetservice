package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;







import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import models.Following;
import models.Tweet;
import models.Tweeter;
import play.mvc.*;
import play.Logger;
import utils.GsonStrategy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

public class FollowingsAPI extends Controller
{
	static Gson gson = new GsonBuilder()
	.setExclusionStrategies(new GsonStrategy())
	.create();

	public static void createFollowing(String id, String followeeId)
	{
		
		Logger.info("create following");
		Tweeter follower = Tweeter.findById(id);
		Logger.info("followeer" + follower); 
		Tweeter followee = Tweeter.findById(followeeId);
		Logger.info("followeee" + followee); 
		follower.follow(followee);
		
		renderJSON(gson.toJson(followee));
	}
	
	public static void deleteFollowing(String id, String followeeId)
	{
		Logger.info("delete following");
		Tweeter follower = Tweeter.findById(id);
		Logger.info("follower" + follower.id);
		Tweeter followee = Tweeter.findById(followeeId);
		Logger.info("follower" + follower.id);
		Logger.info("followee" + followee.id);
		follower.unfollow(followee);
		
		
		List<Following> followings = Tweeters.getFollowings(follower);
		Logger.info("followings" + followings);
		for (Following f : followings)
		{
			Logger.info("following" + f);
			if (f.followee == followee)
				Logger.info("f.followee" + f.followee);
			{
				followings.remove(f);
				f.delete();
				follower.save();
			}
		}
		
		//followings.remove(followee);
		renderJSON (gson.toJson(follower));
		//renderText("success");
	}

	public static void deleteTweet(String id, String tweetId)
	{
		Tweet tweet = Tweet.findById(tweetId);
		if (tweet == null)
		{
			notFound();
		}
		else
		{
			tweet.delete();
			renderJSON (gson.toJson(tweet));
		}
	}  
	public static void deleteAllFollowings()
	{
		Following.deleteAll();
		renderText("success");
	}
	
	/*public static void getAllFollowings(String id)
	{
		Tweeter follower = Tweeter.findById(id);
		List<Following> allFollowings = Tweeters.getFollowings(follower);
		List<Tweeter> followings = new ArrayList<>();
		for (Following following : allFollowings)
		{
			followings.add(following.followee);
		}
		renderJSON(gson.toJson(followings));
		}*/


	public static void getAllFollowings(String id)
	{
		Tweeter follower = Tweeter.findById(id);
		List<Following> allFollowings = Tweeters.getFollowings(follower);
		List<Following> followings = new ArrayList<Following>();
		for (Following following : allFollowings)
		{
				followings.add(following);
		}
		renderJSON(gson.toJson(followings));
	}
	
	public static void getEveryFollowing()
	{
		List<Following> followings = Following.findAll();
		renderJSON(gson.toJson(followings));
	}
}

