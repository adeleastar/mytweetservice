package controllers;

import play.*;
import play.db.jpa.Blob;
import play.mvc.*;
import models.*;

public class Accounts extends Controller
{
	public static void index()
	{
		render();
	}

	public static void signup()
	{
		render();
	}

	public static void register (Tweeter tweeter)
	{
		Logger.info("Registering: " + tweeter.firstName + " " + tweeter.lastName + " " + tweeter.email + " " + tweeter.password);
		tweeter.save();
		login();
	}


	public static void login()
	{
		render();
	}

	public static void logout()
	{
		session.clear();
		index();
	}

	public static void authenticate(String email, String password)
	{
		Logger.info("Attempting to authenticate with " + email + ":" + password);

		Tweeter tweeter = Tweeter.findByEmail(email);
		if ((tweeter != null) && (tweeter.checkPassword(password) == true))
		{
			Logger.info("Successfull authentication of  " + tweeter.firstName + " " + tweeter.lastName);
			session.put("logged_in_userid", tweeter.id);
			Tweets.index();
		}
		else
		{
			Logger.info("Authentication failed");
			login();
		}
	}

	public static Tweeter getCurrentTweeter()
	{
		String userId = session.get("logged_in_userid");
		if (userId == null)
		{
			index();
		}
		Tweeter logged_in_userid = Tweeter.findById(userId);
		Logger.info("getCurrentTweeter: Logged in user is " + logged_in_userid.firstName);
		return logged_in_userid;
	}



}
