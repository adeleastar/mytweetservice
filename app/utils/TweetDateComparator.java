package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import models.Tweet;

public class TweetDateComparator implements Comparator<Tweet>
{
	@Override
	public int compare(Tweet a, Tweet b) 
	{		
		Date aDate = new Date(a.datestamp);
		Date bDate = new Date(b.datestamp);
		return aDate.compareTo(bDate);
	}
}