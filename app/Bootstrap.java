import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import play.*;
import play.db.jpa.Blob;
import play.jobs.*;
import play.libs.MimeTypes;
import play.test.*;
import models.*;

@OnApplicationStart
public class Bootstrap extends Job 
{ 
	public void doJob() throws FileNotFoundException
	{
		Fixtures.deleteDatabase(); 
		Fixtures.loadModels("data.yml");

		String photoName = "homer.gif";
		Blob blob = new Blob ();
		blob.set(new FileInputStream(photoName), MimeTypes.getContentType(photoName));
		Tweeter Homer = Tweeter.findByEmail("homer@simpson.com");
		Homer.profilePicture = blob;
		Homer.save();

		String photoName2 = "marge.gif";
		Blob blob2 = new Blob ();
		blob2.set(new FileInputStream(photoName2), MimeTypes.getContentType(photoName2));
		Tweeter Marge = Tweeter.findByEmail("marge@simpson.com");
		Marge.profilePicture = blob2;
		Marge.save();

		String photoName3 = "grampa.gif";
		Blob blob3 = new Blob ();
		blob3.set(new FileInputStream(photoName3), MimeTypes.getContentType(photoName3));
		Tweeter Grampa = Tweeter.findByEmail("abe@simpson.com");
		Grampa.profilePicture = blob3;
		Grampa.save();

		String photoName4 = "bart.gif";
		Blob blob4 = new Blob ();
		blob4.set(new FileInputStream(photoName4), MimeTypes.getContentType(photoName4));
		Tweeter Bart = Tweeter.findByEmail("bart@simpson.com");
		Bart.profilePicture = blob4;
		Bart.save();

		String photoName5 = "lisa.gif";
		Blob blob5 = new Blob ();
		blob5.set(new FileInputStream(photoName5), MimeTypes.getContentType(photoName5));
		Tweeter Lisa = Tweeter.findByEmail("lisa@simpson.com");
		Lisa.profilePicture = blob5;
		Lisa.save();

		String photoName6 = "maggie.gif";
		Blob blob6 = new Blob ();
		blob6.set(new FileInputStream(photoName6), MimeTypes.getContentType(photoName6));
		Tweeter Maggie = Tweeter.findByEmail("maggie@simpson.com");
		Maggie.profilePicture = blob6;
		Maggie.save();

		String photoName7 = "milhouse.gif";
		Blob blob7 = new Blob ();
		blob7.set(new FileInputStream(photoName7), MimeTypes.getContentType(photoName7));
		Tweeter Milhouse = Tweeter.findByEmail("milhouse@vanhouten.com");
		Milhouse.profilePicture = blob7;
		Milhouse.save();

		String photoName8 = "snowball.gif";
		Blob blob8 = new Blob ();
		blob8.set(new FileInputStream(photoName8), MimeTypes.getContentType(photoName8));
		Tweeter Snowball = Tweeter.findByEmail("snowball@simpson.com");
		Snowball.profilePicture = blob8;
		Snowball.save();
	}
}