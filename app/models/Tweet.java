package models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

import com.google.common.base.Objects;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Tweet extends GenericModel
{
	@Id
	public String id;
	public String tweetText;
	public Long datestamp;
	public String strDate;
	public String charCount;
	public String tweeterTweetText;


	@ManyToOne
	public Tweeter tweeter;

		//public Tweet(){}	

	public Tweet(Tweeter tweeter, String tweetText, Long datestamp, String charCount)
	{
		this.id       = UUID.randomUUID().toString();
		this.tweetText = tweetText;
		this.datestamp  = new Date().getTime(); 
		this.tweeter = tweeter;
		this.charCount = charCount;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (obj instanceof Tweet)
		{
			final Tweet other = (Tweet) obj;
			return Objects.equal(id, other.id) 
					&& Objects.equal(tweetText, other.tweetText) 
					&& Objects.equal(datestamp, other.datestamp)
					&& Objects.equal(charCount, other.charCount);
		}
		else
		{
			return false;
		}
	}	
}