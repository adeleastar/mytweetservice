
$( '.ui.form' ).form( {
	'tweeter.firstName' : {
		identifier : 'tweeter.firstName',
		rules : [
			{
				type : 'empty',
				prompt : 'Please enter your First Name'
			}
		]
	},
	'tweeter.lastName' : {
		identifier : 'tweeter.lastName',
		rules : [
			{
				type : 'empty',
				prompt : 'Please enter your Last Name'
			}
		]
	},
	'tweeter.email' : {
		identifier : 'tweeter.email',
		rules : [
			{
				type : 'empty',
				prompt : 'Please enter your email'
			}
		]
	},
	'tweeter.password' : {
		identifier : 'tweeter.password',
		rules : [
				{
					type : 'empty',
					prompt : 'Please enter a password'
				}, {
					type : 'length[6]',
					prompt : 'Your password must be at least 6 characters'
				}
		]
	}
} );